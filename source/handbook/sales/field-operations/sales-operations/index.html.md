---
layout: handbook-page-toc
title: "Sales Operations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## How to Communicate with Us

Slack: [#sales-support](https://gitlab.slack.com/archives/sales-support)
Salesforce: [@sales-Support](https://gitlab.my.salesforce.com/_ui/core/chatter/groups/GroupProfilePage?g=0F94M000000fy2K)

## Charter

Sales Operations is a part of Field Operations. We aim to help facilitate new and existing processes throughout our field organization via the use of systems, policies, and direct support. Sales Operations is responsible for the following key areas:

*  Territories
*  Go To Market data
*  Bookings
*  Deal Support (see [Deal Desk](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#welcome-to-the-deal-desk-handbook))
*  Sales Support
*  Partner Operations
*  [Customer Success Operations](/handbook/sales/field-operations/sales-operations/customer-success-operations)

## Sales Support - Deal Desk and Sales Operations

The GitLab Sales Support team includes two groups: Deal Desk and Sales Operations.

The Deal Desk team is comprised of in-field resources aligned to the time zones of our sales team. Visit the [Deal Desk Handbook](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#welcome-to-the-deal-desk-handbook) for more information about Sales Support, SLAs, Team Members, Quote Configuration, and more.

## Updating Zuora and Salesforce Quote Templates  
In order to update quote templates that are used in Salesforce, and pulled in from Zuora, please reference the below resources provided by Zuora. 
1.  [General overview to update quote templates](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates)
2.  [Leveraging mail merge fields to update templates](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates/C_Customize_Quote_Template_using_Word_Mail_Merge) - This must be completed in Microsoft word and saved accordingly
3.  [Reference the merge fields that are supported](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates/K_Supported_Merge_Fields_for_Quote_Templates_and_Mapping_to_Salesforce#Charge_Summary.Quote_Rate_Plan_Merge_Fields)
4.  [How to displaty multiple quote charges in a table](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates/E_Customize_Quote_Templates_Using_Microsoft_Word_Mail_Merge)
5.  [Uploading to Zuora and connect to Salesforce](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings)

## Account List Import
Here are the guidelines for requesting account list loads from Sales Operations. Please follow the instructions below.
The SLA for account list loads into Salesforce is 5-7 business days.

**Preparing the list:**
1. Clean up list to remove any duplicates and columns not needed.
2. Update field names to Salesforce compatible values. Only include the required fields listed below.
3. Unless you discuss with us prior, nothing else will be loaded and the extra columns will be ignored in the import.
4. Account Source format: List - Name of Source - Date with no spaces or characters.
5. Create an issue in our [project](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations) using the Account List Import template. Include a link to the list and description of the list load.
6. One Tab per sheet, one list load per sheet / one sheet per issue.
 

**Required Fields:**

| **Label** | **Field Name** | **Data Type** |
| ------ | ------ | ------ |
| Account Source | AccountSource | Picklist |
| Employees | NumberOfEmployees | Number(8,0) |
| Account Name | Name | Name |
| Type | Type | Picklist |
| Account Record Type | RecordType | Picklist |
| Website | Website | URL(255) |
| Billing Street | Billing Street| Address |
| Billing City | Billing City | Address |
| Billing State/Province | Billing State/Province | Address|
| Billing Zip/Postal Code | Billing Zip/Postal Code | Address |
| Billing Country | Billing Country | Address |

**Critical Information:**

* The Sales Operations team member loading the list will need to convert to the Record Type ID before uploading the list.
* Billing address is a combination of fields. All are needed. 
* Billing State can only be included on lists with a billing country of United States or Canada. 


 [**Here is a link for the Billing address formatting required.**](https://docs.google.com/spreadsheets/d/1_FOkc7CHBDaEzPmpoXtkiQE-u-QB_uuJIcAA4mU1gd0/edit?usp=sharing)